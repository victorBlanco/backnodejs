const express = require("express");
const ruta = express.Router();

const { getFinca, getFincas, crearFinca, actualizarFinca, eliminarFinca } = require("../controllers/finca");

ruta
    .route('/')
    .get(getFincas)
    .post(crearFinca)

ruta
    .route('/:id')
    .get(getFinca)
    .put(actualizarFinca)
    .delete(eliminarFinca)


module.exports = ruta;