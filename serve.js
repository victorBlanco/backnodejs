const dotenv = require('dotenv');
const express = require('express');
const finca = require('./rutas/finca');
const morgan = require('morgan');


dotenv.config({ path: './config/config.env' });

//crear aplicacion de express
const app = express();

if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));

}


app.use('/api/Finca', finca);


const PORT = process.env.PORT || 5000

app.listen(PORT, console.log('servidor se ejecuta en ambiente', process.env.NODE_ENV));