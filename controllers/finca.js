exports.getFincas = (req, res, next) => {
    res.status(200).json({ status: 200, mensaje: 'se ha conectado correctamente y su proceso fue exitoso' });

}

exports.getFinca = (req, res, next) => {
    res.status(200).json({ status: 200, mensaje: 'se presenta informacion de la finca' });
}

exports.crearFinca = (req, res, next) => {
    res.status(200).json({ status: 200, mensaje: 'se ha registrado exitosamente la finca' });

}
exports.actualizarFinca = (req, res, next) => {
    res.status(200).json({ status: 200, mensaje: 'se ha actualizado exitosamente la finca' });

}

exports.eliminarFinca = (req, res, next) => {
    res.status(200).json({ status: 200, mensaje: 'se ha Eliminado  exitosamente la finca' });
}