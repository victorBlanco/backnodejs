const mysql = require('mysql');

var conexion = mysql.createConnection({
    host: 'localhost',
    database: 'smartcrop',
    user: 'root',
    password: 'root'
});

conexion.connect(function(error) {
    if (error) {
        throw error;
    } else {
        console.log('CONEXION EXITOSA');
    }
});

conexion.end();